<?php

use App\Http\Controllers\BlogPostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BlogPostController::class, 'index'])->name('list');
Route::get('/blog/{blogPost}', [BlogPostController::class, 'show'])->name('show');
Route::get('/blog', [BlogPostController::class, 'index'])->name('list');


Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/blog/create/post', [BlogPostController::class, 'create'])->name('create'); //shows create post form
    Route::post('/blog/create/post', [BlogPostController::class, 'store'])->name('store'); //saves the created post to the databse
    Route::get('/blog/{blogPost}/edit', [BlogPostController::class, 'edit'])->name('edit'); //shows edit post form
    Route::put('/blog/{blogPost}/edit', [BlogPostController::class, 'update'])->name('update'); //commits edited post to the database
    Route::delete('/blog/{blogPost}', [BlogPostController::class, 'destroy'])->name('delete'); //deletes post from the database
    Route::post('/blog/{blogPost}/review', [BlogPostController::class, 'review'])->name('review'); //add review to post

});
