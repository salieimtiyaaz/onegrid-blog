@extends('layouts.main')

@section('content')

    @foreach($posts as $post)
    <!-- Post preview-->
    <div class="post-preview">
        <a href="{{route('show', $post->id)}}">
            <h2 class="post-title">{{ucfirst($post->title)}}</h2>
            <h3 class="post-subtitle text-truncate">{!! $post->body !!}</h3>
        </a>
        <p class="post-meta">
            Posted by
            on {{$post->updated_at}}
        </p>
    </div>
    <!-- Divider-->
    <hr class="my-4" />
    <!-- Post preview-->
    @endforeach

    {{ $posts->onEachSide(3)->links() }}
@endsection
