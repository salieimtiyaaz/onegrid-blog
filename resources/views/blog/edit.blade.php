@extends('layouts.main')

@push('page-css')
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
@endpush

@push('page-js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <script>
        $(document).ready(function () {

            var options = {
                theme: 'snow'
            };
            var editor = new Quill('.editor', options);

            editor.root.innerHTML = `{!! $post->body  !!}`


            $("#form").submit(function () {
                $(".body").val(editor.root.innerHTML);
            });
        });
    </script>
@endpush

@section('content')

    <a href="{{route('list')}}" class="btn btn-secondary mb-3">Return</a>

    <h2 class="mb-3 text-secondary">Update Post</h2>

    <form id="form" method="POST" action="{{route('update', $post->id)}}">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label>Title</label>
            <input type="text" value="{{$post->title}}" name="title" class="form-control" placeholder="Title">
        </div>
        <div class="form-group mh-100 mt-4">
            <label>Content</label>
            <div class="editor"></div>
            <textarea class="body d-none" name="body"></textarea>
        </div>

        <button type="submit" class="btn btn-primary mt-5">Publish</button>
    </form>
@endsection
