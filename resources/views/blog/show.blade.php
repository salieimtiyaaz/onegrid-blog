@extends('layouts.main')

@section('content')

    <div class="btn-group" role="group">
        <a href="{{route('list')}}" type="button" class="btn btn-secondary">Return</a>
        @auth
        @if($post->user_id == auth()->user()->id)
        <a href="{{route('edit', $post->id)}}" type="button" class="btn btn-secondary">Edit</a>
        <form id="delete-frm" class="" action="{{route('delete', $post->id)}}" method="POST">
            @method('DELETE')
            @csrf
            <button class="btn btn-secondary">Delete Post</button>
        </form>
        @endif
        @endauth
    </div>

    <!-- Post preview-->
    <div class="post-preview">
        <a href="{{route('show', $post->id)}}">
            <h2 class="post-title">{{ucfirst($post->title)}}</h2>
            <h3 class="post-subtitle text-truncate">{!! $post->body !!}</h3>
        </a>
        <p class="post-meta">
            Posted by
            on {{$post->updated_at}}
        </p>
    </div>
    <!-- Divider-->
    <hr class="my-4" />
    <!-- Post preview-->

    <h3 class="post-title mb-3">Review <span class="badge badge-danger text-primary">Rating: {{$post->averageRating(1)->first()}}</span></h3>

    @auth
        @if($post->user_id != auth()->user()->id)
    <form method="post" action="{{route('review', $post->id)}}">
        @csrf
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="rating" id="inlineRadio1" value="1">
            <label class="form-check-label" for="inlineRadio1">1</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="rating" id="inlineRadio2" value="2">
            <label class="form-check-label" for="inlineRadio2">2</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="rating" id="inlineRadio2" value="3">
            <label class="form-check-label" for="inlineRadio2">3</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="rating" id="inlineRadio2" value="4">
            <label class="form-check-label" for="inlineRadio2">4</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="rating" id="inlineRadio2" value="5">
            <label class="form-check-label" for="inlineRadio2">5</label>
        </div>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Comment</label>
            <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <button class="btn btn-primary mt-3" type="submit">Review</button>
    </form>
        @endif
    @endauth

    <hr class="my-4" />

    <h3 class="post-title mb-3">Reviews</h3>

    @foreach($post->getAllRatings($post->id) as $comment)
        <h6 class="post-title mb-3 text-primary">Rating: {{$comment->rating}} <span class="float-right text-dark">{{$comment->updated_at}}</span></h6>

        <blockquote class="quote-card">
            <p>{{$comment->body}}</p>
        </blockquote>
        <hr class="my-4" />
    @endforeach
@endsection
