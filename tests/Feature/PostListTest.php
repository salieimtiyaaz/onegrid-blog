<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostShowTest extends TestCase
{
    /**
     * @return void
     */
    public function test_post_show()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertViewIs('blog.index');


    }
}
