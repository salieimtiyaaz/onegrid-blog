<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\BlogPost;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class BlogPostController
 * @package App\Http\Controllers
 */
class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $posts = BlogPost::orderBy('updated_at', 'desc')->paginate(3);

        return view('blog.index', [
            'posts' => $posts,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePostRequest $request
     * @return RedirectResponse
     */
    public function store(StorePostRequest $request): RedirectResponse
    {
        $newPost = new BlogPost();
        $newPost->title = $request->get('title');
        $newPost->body = $request->get('body');
        $newPost->user_id = auth()->user()->id;
        $newPost->save();
        return redirect()->route('show', $newPost->id);
    }

    /**
     * Display the specified resource.
     *
     * @param BlogPost $blogPost
     * @return Application|Factory|View
     */
    public function show(BlogPost $blogPost)
    {
        $avgRating = $blogPost->averageRating();

        return view('blog.show', [
            'post' => $blogPost,
            'avgRating' => $avgRating,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BlogPost $blogPost
     * @return Application|Factory|View
     */
    public function edit(BlogPost $blogPost)
    {
        return view('blog.edit', ['post' => $blogPost]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePostRequest $request
     * @param BlogPost $blogPost
     * @return RedirectResponse
     */
    public function update(UpdatePostRequest $request, BlogPost $blogPost): RedirectResponse
    {
        $blogPost->update([
            'title' => $request->get('title'),
            'body' => $request->get('body')
        ]);

        return redirect()->route('show', $blogPost->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BlogPost $blogPost
     * @return RedirectResponse
     */
    public function destroy(BlogPost $blogPost): RedirectResponse
    {
        if (auth()->user()->id == $blogPost->user_id) {
            $blogPost->delete();
            return redirect()->route('list');
        }

        abort('403');
    }

    /**
     * @param Request $request
     * @param BlogPost $blogPost
     * @return RedirectResponse
     */
    public function review(Request $request, BlogPost $blogPost): RedirectResponse
    {
        $blogPost->rating([
            'title' => '',
            'body' => $request->get('comment'),
            'rating' => $request->get('rating'),
        ], auth()->user());

        return redirect()->back();
    }

}
