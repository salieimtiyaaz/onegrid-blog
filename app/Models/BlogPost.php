<?php

namespace App\Models;

use Codebyray\ReviewRateable\Contracts\ReviewRateable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model implements ReviewRateable
{
    use HasFactory, \Codebyray\ReviewRateable\Traits\ReviewRateable;

    protected $table = 'blog_posts';

    protected $fillable = ['title', 'body', 'user_id'];

}
