const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('resources/theme/assets/img', 'public/img')
mix.copy('resources/theme/assets/favicon.ico', 'public/favicon.ico')
mix.copy('resources/theme/css/styles.css', 'public/css/styles.css')
mix.copy('resources/theme/js/scripts.js', 'public/js/scripts.js')

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();
